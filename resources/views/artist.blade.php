@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Alle Künstler</div>

                <table class="table table-striped">

                @foreach ($artists as $artist)

                <tr> 
                    <td> {{ $artist->id }} </td>  
                    <td>{{ $artist->name }}</td> 
                    <td> <a href="{{ url('album/show/'.$artist->id) }}">Alben anzeigen</a> </td> 
                    <td> <a href="{{ url('artist/delete/'.$artist->id) }}">Löschen</a> </td> 
                </tr>

                @endforeach

                </table>

            </div>

        </div>
    </div>
</div>
@endsection
