@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tracks</div>

                <div class="panel-body">
                <h1>Track hinzufügen</h1>

                    <table class="table table-striped">

                    {!! Form::open(array('url' => array('track/new/'))) !!}

                    <tr> <td> {!! Form::label('track_name', 'Liedname'); !!} </td>
                    <td> {!! Form::text('track_name') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('track_length', 'Länge'); !!} </td>
                    <td>{!! Form::text('track_length') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('track_price', 'Preis'); !!} </td>
                    <td> {!! Form::text('track_price') !!}<br/> </td> </tr>

                    {!! Form::hidden('invisible', '1') !!}
                    
                    <tr> <td> {!! Form::submit('Speichern!') !!} </td> </tr>

                    {!! Form::close() !!}

                    </table>

                </div>

                <div class="panel-body">
                    <h1>Tracks</h1>

                    <table class="table table-striped">

                        @foreach ($tracks as $track)

                        <tr>
                            <td>ID</td>
                            <td>Track_Name</td>
                            <td>Länge</td>
                            <td>Preis</td>
                            <td></td>
                        </tr>

                            <tr>  
                                <td> {!! $track->id !!} </td>                              
                                <td> {!! $track->track_name !!} </td>  
                                <td> {!! $track->track_length !!}</td> 
                                <td> {!! $track->track_price !!} </td>
                                <td> <a href="{{ url('track/delete/'.$track->id) }}">Track löschen</a> </td> 
                            </tr>

                        @endforeach

                    </table>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
