@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Album</div>

                <div class="panel-body">
                <h1>Album anlegen</h1>

                    <table class="table table-striped">

                    {!! Form::open(array('action' => array('AlbumController@newAlbum'))) !!}

                    <tr> <td> {!! Form::label('title', 'Titel'); !!} </td>
                    <td> {!! Form::text('title') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('price', 'Preis'); !!} </td>
                    <td>{!! Form::text('price') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('front_cover', 'Vorderseite'); !!} </td>
                    <td> {!! Form::text('front_cover') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('back_cover', 'Rückseite'); !!} </td>
                    <td> {!! Form::text('back_cover') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::label('length', 'Gesamtlänge'); !!} </td>
                    <td> {!! Form::text('length') !!}<br/> </td> </tr>
                    <tr> <td> {!! Form::submit('Speichern!') !!} </td> </tr>


                    {!! Form::hidden('invisible', $id) !!}
                    {!! Form::close() !!}

                    </table>

                </div>

                <div class="panel-body">
                    <h1>Alben</h1>

                    <table class="table table-striped">

                        @foreach ($albums as $album)

                            <tr>    
                                <td> {!! $album->id !!} </td>  
                                <td> {!! $album->title !!}</td> 
                                <td> {!! $album->price !!} </td>
                                <td> {!! $album->front_cover !!} </td>
                                <td> {!! $album->back_cover !!} </td>
                                <td> {!! $album->length !!} </td>
                                <td> <a href="{{ url('cart/add/1') }}">Album kaufen</a> </td>  
                                <td> <a href="{{ url('track/'.$album->id) }}">Tracks anzeigen</a> </td> 
                                <td> <a href="{{ url('album/delete/'.$album->id.'/'.$album->artist_id) }}">Album löschen</a> </td> 
                            </tr>

                        @endforeach

                    </table>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
