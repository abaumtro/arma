@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Warenkorb</div>

                <div class="panel-body">

                    <table class="table table-striped">

                        @foreach ($albums as $album)

                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Preis</td>
                                <td>Länge</td>
                                <td>Anzahl</td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr>    
                                <td> {!! $album->id !!} </td>  
                                <td> {!! $album->title !!}</td> 
                                <td> {!! $album->price !!} </td>
                                <td> {!! $album->length !!} </td>
                                <td> {!! $album->amount !!} </td>
                                <td> <a href="{{ url('cart/'.$album->id) }}">Hinzufügen</a> </td>  
                                <td> <a href="{{ url('track/'.$album->id) }}">Entfernen</a> </td>
                            </tr>

                        @endforeach

                    </table>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
