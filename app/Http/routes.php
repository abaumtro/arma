<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

/* Beispiel */

Route::get('/artist', 'ArtistController@getAllArtist');
Route::post('/artist', 'ArtistController@newArtist');
Route::get('/artist/delete/{id?}', 'ArtistController@deleteArtist');

Route::get('/album/show/{id?}', 'AlbumController@getAllAlbums');
Route::get('/album/delete/{id?}/{artist_id?}', 'AlbumController@deleteAlbum');
Route::post('/album', 'AlbumController@newAlbum');

Route::get('/album/showTracks/{album_id?}', 'AlbumController@getTracks');

Route::get('/track/{album_id?}', 'TrackController@getAllTracks');
Route::get('/track', 'TrackController@test');
Route::post('/track/new', 'TrackController@newTrack');


Route::get('/cart/{user_id?}', 'CartController@getCart');
Route::get('/cart/add/{user_id?}', 'CartController@addAlbum');