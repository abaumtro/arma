<?php

namespace Seminar2\Http\Controllers;

use Request;
use Seminar2\Http\Requests;
use Seminar2\Models\Album;
use Seminar2\Models\Artist;

class AlbumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function getAllAlbums($id = null)
    {
        if ($id) {
        
            $artist = Artist::find($id);
            if ($artist)
            {
                $albums = $artist->album;
            }

            return view('album', ['albums' => $albums, 'id' => $id]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('album');
    }

    public function newAlbum() 
    {
        $album = new Album;
        $album->artist_id = Request::input('invisible');
        $album->price = Request::input('price');
        $album->title = Request::input('title');
        $album->front_cover = Request::input('front_cover');
        $album->back_cover = Request::input('back_cover');
        $album->length = Request::input('length');
        $album->save();

        return redirect('album/show/'.Request::input('invisible'));
    }

    public function deleteAlbum($id = null, $artist_id = null)
    {

        if ($id) 
        {
            $tmp = Album::find($id);
            $tmp->delete();
            if ($artist_id) 
            {
                return redirect('album/show/'.$artist_id);            
            } else {
                return redirect('/artist'); 
            }
        }
    }

    public function getTracks($album_id = null) 
    {
        if ($album_id)
        {
            return redirect('track/'.$album_id);
        } else {
            return view('artist');
        }
    }
}
