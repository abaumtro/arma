<?php

namespace Seminar2\Http\Controllers;

use Request;
use Seminar2\Http\Requests;
use Seminar2\Models\track;
use Seminar2\Models\album;
use Seminar2\Models\artist;

class TrackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function getAllTracks($album_id = null)
    {
        if ($album_id) {
        
            $album = Album::find($album_id);
            if ($album)
            {
                $tracks = $album->track;
            }

            return view('track', ['tracks' => $tracks, 'id' => $album_id]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('track');
    }

    public function newTrack() 
    {
        $track = new Track;
        $track->artist_id = Request::input('artist_id');
        $track->track_name = Request::input('track_name');
        $track->track_length = Request::input('track_length');
        $track->track_price = Request::input('track_price');
        $track->save();

        return redirect('track/1');
    }

    public function deletetrack($id = null)
    {
        $tmp = track.find(id);
    }

    public function test()
    {
        echo "string";
    }
}
