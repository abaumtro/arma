<?php

namespace Seminar2\Http\Controllers;

use Request;
use Seminar2\Http\Requests;
use Seminar2\Models\Artist;

class ArtistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function getAllArtist()
    {
    	$artists = Artist::all();

        return view('artist', ['artists' => $artists]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artist');
    }

    public function newArtist() 
    {
        $artist = new Artist;
        $artist->name = Request::input('name');
        $artist->save();

        return redirect('artist');
    }

    public function deleteArtist($id = null)
    {
        $tmp = Artist::find($id);
        $tmp->delete();
        return redirect('artist');
        
    }
}
