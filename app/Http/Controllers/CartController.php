<?php

namespace Seminar2\Http\Controllers;

use Request;
use Seminar2\Http\Requests;
use Seminar2\Models\track;
use Seminar2\Models\album;
use Seminar2\Models\artist;
use Seminar2\Models\cart;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function getCart($user_id = null)
    {

        if ($user_id) {
        
            $cart = Cart::find($user_id);
            if ($cart)
            {
                $albums = $cart->album;
            }

            return view('cart', ['albums' => $albums, 'id' => $user_id]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function addAlbum($user_id = null) 
    {
        $album_cart = new Album_Cart;
        
        $album_cart->user_id = $user_id;
        $album_cart->save();

        return redirect('cart/'.$user_id);
    }
}
