<?php

namespace Seminar2\Models;


use Illuminate\Database\Eloquent\Model as Model;

class Track extends Model
{
	public function artist() {
		return $this->belongsTo('Seminar2\Models\Artist');
	}

	public function album() {
		return $this->belongsTo('Seminar2\Models\Album');
	}

}
