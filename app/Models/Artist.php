<?php

namespace Seminar2\Models;


use Illuminate\Database\Eloquent\Model as Model;

class Artist extends Model
{
    public function album() {
        return $this->hasMany('Seminar2\Models\Album');
    }

    public function tracks() {
        return $this->hasMany('Seminar2\Models\Track');
    }

}
