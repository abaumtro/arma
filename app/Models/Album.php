<?php

namespace Seminar2\Models;


use Illuminate\Database\Eloquent\Model as Model;

class Album extends Model
{
	public function artist() {
		return $this->belongsToMany('Seminar2\Models\Artist');
	}

	public function track() {
		return $this->belongsToMany('Seminar2\Models\Track');
	}

	    public function cart() {
        return $this->belongsToMany('Seminar2\Models\Cart');
    }

}
