<?php

namespace Seminar2\Models;


use Illuminate\Database\Eloquent\Model as Model;

class Cart extends Model
{

    public function user() {
        return $this->belongsToMany('Seminar2\Models\User');
    }

    public function album() {
        return $this->belongsToMany('Seminar2\Models\Album');
    }
}
