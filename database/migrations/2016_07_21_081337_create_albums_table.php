<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    Schema::create('albums', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('artist_id')->unsigned()->nullable();
        $table->foreign('artist_id')->references('id')->on('artists');        
        $table->string('price');
        $table->string('title');
        $table->string('front_cover');
        $table->string('back_cover');
        $table->string('length');
        $table->rememberToken();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('albums');
    }
}
