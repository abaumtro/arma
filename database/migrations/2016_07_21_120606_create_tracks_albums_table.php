<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    Schema::create('album_track', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('album_id')->unsigned()->nullable();
        $table->foreign('album_id')->references('id')->on('albums');         
        $table->integer('track_id')->unsigned()->nullable();
        $table->foreign('track_id')->references('id')->on('tracks');
        $table->timestamps();

    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('album_track');
    }
}
